import java.util.HashSet;
import java.util.Scanner;

public class start { 
    
    private static long startTime;
    private static long totalRound;
    private static long totalMessages;


    public static HashSet<Integer> genRing(int MINVALUE, int MAXVALUE, int VALUES){
        int range = MAXVALUE - MINVALUE + 1;

        HashSet<Integer> ring = new HashSet<Integer>();
        while(ring.size() < VALUES){
            int rand = (int)(Math.random() * range) + MINVALUE;
            ring.add(rand);
        }
        return ring;
    }

    public static void LCR(){
        System.out.println("=== Asynchronous-Start and Terminating LCR Algorithm ===");

        Scanner scanner = new Scanner( System.in );
        try{
            System.out.println("Enter maximal value that will be generated: ");
            int MaxValue = scanner.nextInt();

            System.out.println("Enter size of ring: ");
            int RingSize = scanner.nextInt();

            
            HashSet<Integer> g = genRing(1, MaxValue, RingSize); // Min Value, Max Value, Size of ring
            System.out.println(g);
            
            for(int i = 0; i < g.size(); i++){
                LCR mything = new LCR(i,g);
                mything.start();
                mything.join(1000);
                totalMessages += mything.getmessages();
            } 
            System.out.println("Total messages: " + totalMessages);

            scanner.close();
        }catch(Exception exception){

        }
    }

    public static void LEA(){
        // Testing purpose
        //int sor = 1; // Size of Ring
        //int subsiz = 100; //
        //int subam = 10; // Amount of subrings

        System.out.println("=== Leader-Election Algorithm for Rings of Rings ===");
        Scanner scanner = new Scanner( System.in );
        try{
            System.out.println("Enter maximal value that will be generated: ");
            int MaxValue = scanner.nextInt();
            //int MaxValue = 9999; Change it for testing

            System.out.println("Enter size of main ring: ");

            int RingSize = scanner.nextInt();
            //int RingSize = sor; Change it for testing

            HashSet<Integer> g = genRing(1, MaxValue, RingSize); // Min Value, Max Value, Size of ring

            System.out.println("Amount of subnetwork rings: ");
            int d = scanner.nextInt();
            //int d = subam; Change it for testing
            for(int x = d; x > 0; x--){
                System.out.println("Main ring generated: ");
                System.out.println(g);
                System.out.println("Which value from the ring should be a subnetwork in main ring?\nThis will generate new leader which will replace the current value with new one: ");
                int sI = scanner.nextInt();
                //int sI = g.iterator().next(); Change it for testing
                
                g.remove(sI);

                System.out.println("Size of subnetwork: ");
                int subnetworkSize = scanner.nextInt();
                //int subnetworkSize = subsiz; // Change it for testing
                
                HashSet<Integer>  gen = genRing(1, MaxValue, subnetworkSize);
                //System.out.println("Subnet" + gen);

                for(int i = 0; i < gen.size(); i++){
                    subnetwork sub = new subnetwork(i,gen);
                    sub.start();
                    sub.join();
                    totalMessages += sub.getmessages();
                    int value = sub.getValue();
                    if(value > 0){
                        g.add(value);
                    }
                } 
            }
                for(int i = 0; i < g.size(); i++){
                    LCR mything = new LCR(i,g);
                    mything.start();
                    mything.join(1);
                    totalMessages += mything.getmessages();
                } 
                System.out.println("Total messages: " + totalMessages);

            }catch(Exception exception){

            }

            scanner.close();
    }

    public static void main(String[] args){
        Scanner x = new Scanner( System.in );
        try{
            System.out.println("Assignment 1 - Franciszek Wojciechowski");
            System.out.println("\nWhich algorithm do you want to use?\n   1) LCR \n   2) Leader-Election \nYour choice: ");
            int choice = x.nextInt();

            if(choice == 1){
                startTime = System.nanoTime();
                LCR();
                long endTime   = System.nanoTime();

                long totalTime = endTime - startTime;
                System.out.println("Time: " + totalTime);
                
            }else if(choice == 2){
                startTime = System.nanoTime();
                LEA();
                long endTime   = System.nanoTime();

                long totalTime = endTime - startTime;
                System.out.println("Time: " + totalTime);

            }else{
                System.out.println("Try again");
            }
            x.close();
        }catch(Exception exception){
            
        }
    }
}