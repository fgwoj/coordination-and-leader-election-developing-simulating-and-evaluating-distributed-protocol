import java.util.HashSet;

public class LCR extends Thread{ 
    private int threadNum;
    private volatile int messages;
    private volatile int roundsTotal;
    private HashSet<Integer> ring;

    public static int lcrAlgorithm(int inID, int myID, int leader){
        int sendID = myID;
        //String status = "unknown";
        if(inID > myID){
            sendID = inID;
            leader = sendID;
        }else if(inID == myID){
            leader = myID;
        }else if(inID < myID){
            // Do nothing
        }
        return leader;
    }

    public LCR (int threadNum, HashSet<Integer> ring){
        this.threadNum = threadNum;
        this.ring = ring;
    }
    @Override
    public void run(){
        Object [] f = ring.toArray();
        int numProcessor = threadNum;
        int realPosition = 0;
        int ringSize = ring.size();
        int round = 0;
        int leader = (int) f[threadNum];
        for(int i = 0; i < ringSize  + 1; i++){

            try {
                round++;
                if(i > 0){
                    threadNum += 1;
                }   
                if(realPosition < ringSize){
                    if(threadNum >= ringSize){
                        threadNum = 0;
                        realPosition = 0;
                    }else{
                        realPosition += 1;
                    }
                }
                int currentad = (int) f[threadNum];
                int prevad = 0;
                if(threadNum == 0){
                    prevad = (int) f[ringSize-1]; // Last element of a ring
                }else{
                    prevad = (int) f[threadNum-1]; // Next element of a ring
                }

                leader = lcrAlgorithm(prevad, leader, leader);
                if(leader < currentad || round == ringSize+2){
                    System.out.println("Rounds: " + round + " Processor: " + numProcessor);
                    break;
                }else{
                    messages += 1;
                    roundsTotal += 1;
                    Thread.sleep(5);
                }

            }
            catch (InterruptedException e) {

            }
        }
    }
    public int getmessages(){
        return messages;
    }
    public int getRounds(){
        return roundsTotal;
    }


    public static void main(String[] args){

    }
}
